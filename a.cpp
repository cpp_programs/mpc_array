/*

http://stackoverflow.com/questions/14281364/clearing-and-deleting-multiprecision-variables

g++ -Wall a.cpp -lmpc -lmpfr -lgmp

*/


using namespace std;
#include <gmp.h>
#include <mpfr.h>
#include <mpc.h>

int main() {

    int i;
    mpc_t *mpcarray;

    // init 
    mpcarray=new mpc_t[3];
    for(i=0;i<3;i++) mpc_init2(mpcarray[i], 64);

    // set
    for(i=0;i<3;i++) mpc_set_ui_ui(mpcarray[i],i, i, 64);


    //
   for(i=0;i<3;i++){
   mpc_out_str (stdout, 10 , 0, mpcarray[i], MPC_RNDNN); printf ("  \n");
   }

    // free memory
    for(i=0;i<3;i++) mpc_clear(mpcarray[i]);
    delete [] mpcarray;

    return 0;
}
